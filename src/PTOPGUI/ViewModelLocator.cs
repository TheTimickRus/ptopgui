﻿using GalaSoft.MvvmLight.Ioc;
using PTOPGUI.Modules.MainModule.ViewModel;

namespace PTOPGUI
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            SimpleIoc.Default.Register<MainViewModel>();
        }

        public MainViewModel MainViewModel => SimpleIoc.Default.GetInstance<MainViewModel>();
    }
}
