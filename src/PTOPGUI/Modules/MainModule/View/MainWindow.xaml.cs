﻿using System;
using System.Windows;
using PTOPGUI.Properties;

namespace PTOPGUI.Modules.MainModule.View
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            // Загружаем настройки
            LoadSettings();
        }

        private void LoadSettings()
        {
            Width = Settings.Default.MainWindowSize.Width;
            Height = Settings.Default.MainWindowSize.Height;
        }

        private void MainWindow_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Settings.Default["MainWindowSize"] = new System.Drawing.Size(Convert.ToInt32(e.NewSize.Width), Convert.ToInt32(e.NewSize.Height));
            Settings.Default.Save();
        }
    }
}
