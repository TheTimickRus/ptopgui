﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using PTOPGUI.Properties;

namespace PTOPGUI.Modules.MainModule.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly string _resPath = Path.Combine(Environment.CurrentDirectory, "Resources");

        public string InpText { get; set; }
        public string OutText { get; set; }
        public bool CopyButtonEnabled => !string.IsNullOrEmpty(OutText);
        public bool FormatButtonEnabled => !string.IsNullOrEmpty(InpText);

        public MainViewModel()
        {
            if (!Directory.Exists(_resPath))
            {
                Directory.CreateDirectory(_resPath);
            }

            if (!File.Exists(Path.Combine(_resPath, "ptop.exe")))
            {
                File.WriteAllBytes(Path.Combine(_resPath, "ptop.exe"), Resources.ptop);
            }

            if (!File.Exists(Path.Combine(_resPath, "ptop.cfg")))
            {
                File.WriteAllBytes(Path.Combine(_resPath, "ptop.cfg"), Resources.ptopCfg);
            }
        }

        public ICommand BClearCommand => new RelayCommand(
            () =>
            {
                InpText = string.Empty;
                OutText = string.Empty;
            });

        public ICommand BCopyCommand => new RelayCommand(
            () =>
            {
                Clipboard.Clear();
                Clipboard.SetDataObject(OutText);
            });

        public ICommand BFormatCommand => new RelayCommand(
            async () =>
            {
                var inpFilename = Path.Combine(_resPath, "inpTmp");
                var outFilename = Path.Combine(_resPath, "outTmp");
                File.Delete(inpFilename);
                File.Delete(outFilename);

                File.WriteAllText(inpFilename, InpText);

                var processFilename = Path.Combine(_resPath, "ptop.exe");
                var args = $"-c {Path.Combine(_resPath, "ptop.cfg")} {inpFilename} {outFilename}";
                await Task.Factory.StartNew(() =>
                {
                    var proc = new Process
                    {
                        StartInfo = new ProcessStartInfo(processFilename, args)
                        {
                            CreateNoWindow = true,
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                    };

                    proc.Start();
                    proc.WaitForExit();
                });

                OutText = File.ReadAllText(outFilename);

                File.Delete(inpFilename);
                File.Delete(outFilename);
            });
    }
}
